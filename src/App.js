import React, { Component } from 'react';
import './App.css';
import { BrowserRouter , Route } from "react-router-dom";

import Feed from './Feed';
import SingleJoke from './SingleJoke';
class App extends Component {
  render() {
    return (
<BrowserRouter >
  <div>

   <Route exact   path={"/"} component={Feed}  /> 

   <Route path={"/joke/"}  component={SingleJoke}/> 
   </div>

</BrowserRouter>
    );
  }
}

export default App;
